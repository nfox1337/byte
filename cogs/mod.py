import datetime
import psutil
import random
import asyncio
import aiohttp
import discord
from discord.ext import commands
import load
import os

class mod:
    
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions(manage_nicknames=True)
    async def nick(self, ctx, *, name : str):
        '''Changes the bot's nickname'''
        if name == 'reset':
            await ctx.guild.me.edit(nick='')
            embed=discord.Embed(color=load.__embedgreen__, description=f'Nickname successfully reseted')
            await ctx.send(embed=embed)
        else:
            try:
                await ctx.guild.me.edit(nick=name)
            except Exception as e:
                raise e
            else:
                embed=discord.Embed(color=load.__embedgreen__, description=f'Nickname successfully changed to `{name}`')
                await ctx.send(embed=embed)

    @commands.command(aliases=['clean', 'cleanup'])
    @commands.has_permissions(manage_messages=True)
    async def clear(self, ctx, number : int):
        '''Clears messages'''
        try:
            clearnumber = 0
            messages = await ctx.channel.purge(limit=number + 1)
            for message in messages:
                clearnumber = clearnumber + 1
                message.delete()
        except Exception as e:
            raise e
        else:
            embed=discord.Embed(color=load.__embedblue__, description=f'<{load.__staff__}> `{clearnumber - 1}` messages deleted')
            await ctx.send(embed=embed, delete_after=7.5)

    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx, member : discord.Member, *, reason : str = None):
        '''Kicks a member, i guess'''
        try:
            await member.kick()
        except Exception as e:
            raise e
        else:
            await ctx.message.add_reaction(load.__success__)
            embed=discord.Embed(color=load.__embedblue__)
            embed.set_author(name='Kick')
            embed.add_field(name='User', value=f'{member.name}#{member.discriminator} ({member.id})', inline=False)
            embed.add_field(name='Reason', value=reason, inline=False)
            embed.add_field(name='Responsible moderator', value=f'{ctx.author.name}#{ctx.author.discriminator}', inline=False)
            channel = discord.utils.get(ctx.guild.text_channels, name='log')
            await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, member : discord.Member, *, reason : str = 'No reason given'):
        '''Bans a member'''
        try:
            await member.ban()
        except Exception as e:
            raise e
        else:
            await ctx.message.add_reaction(load.__success__)
            embed=discord.Embed(color=load.__embedblue__)
            embed.set_author(name='Ban')
            embed.add_field(name='User', value=f'{member.name}#{member.discriminator} ({member.id})', inline=False)
            embed.add_field(name='Reason', value=reason, inline=False)
            embed.add_field(name='Responsible moderator', value=f'{ctx.author.name}#{ctx.author.discriminator}', inline=False)
            channel = discord.utils.get(ctx.guild.text_channels, name='log')
            await channel.send(embed=embed)

def setup(bot):
    bot.add_cog(mod(bot))