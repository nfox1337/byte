try:
    from config.config import __prefix__, __token__, __logchannel__
    from config.cogs import __cogs__
    from config.blacklist import __blacklist__
    from config.colors import __embedblue__, __embedred__, __embedgreen__, __embedgray__
    from config.emoji import __poll_emotes__, __success__, __error__, __online__, __away__, __dnd__, __offline__, __streaming__, __invisible__, __youtube__, __discord__, __staff__, __warning__
except ImportError:
    pass