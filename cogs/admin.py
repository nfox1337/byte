import datetime
import psutil
import random
import asyncio
import aiohttp
import discord
from discord.ext import commands
import load
import os

from ctypes.util import find_library

class admin:

    def __init__(self, bot):
        self.bot = bot

    @commands.command(hidden=True)
    @commands.is_owner()
    async def load(self, ctx, *, cog : str):
        try:
            self.bot.load_extension(f'cogs.{cog}')
        except Exception as e:
            raise e
        else:
            embed=discord.Embed(color=load.__embedgreen__)
            embed.set_author(name=f'Loaded {cog} cog')
            await ctx.send(embed=embed)

    @commands.command(hidden=True)
    @commands.is_owner()
    async def unload(self, ctx, *, cog : str):
        try:
            self.bot.unload_extension(f'cogs.{cog}')
        except Exception as e:
            raise e
        else:
            embed=discord.Embed(color=load.__embedgreen__)
            embed.set_author(name=f'Unloaded {cog} cog')
            await ctx.send(embed=embed)

    @commands.command(hidden=True)
    @commands.is_owner()
    async def reload(self, ctx, *, cog : str):
        if cog == 'all':
            try:
                for cog in load.__cogs__: 
                    self.bot.unload_extension(f'{cog}')
                    self.bot.load_extension(f'{cog}') 
            except Exception as e:
                raise e
            else:
                embed=discord.Embed(color=load.__embedgreen__)
                embed.set_author(name='Reloaded all cogs')
                await ctx.send(embed=embed)       
        else:
            try:
                self.bot.unload_extension(f'cogs.{cog}')
                self.bot.load_extension(f'cogs.{cog}')
            except Exception as e:
                raise e
            else:
                embed=discord.Embed(color=load.__embedgreen__)
                embed.set_author(name=f'Reloaded {cog} cog')
                await ctx.send(embed=embed)
        
    @commands.command(hidden=True, aliases=['eval'])
    @commands.is_owner()
    async def _eval(self, ctx, *, code : str):
        embed=discord.Embed(color=load.__embedgreen__)
        embed.add_field(name='Result', value=f'```{eval(code)}```')
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(admin(bot))