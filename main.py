import datetime
import psutil
import random
import asyncio
import aiohttp
import discord
from discord.ext import commands
import load
import os

bot = commands.Bot(command_prefix=load.__prefix__)

for cog in load.__cogs__:
    try:
        bot.load_extension(cog)
    except Exception:
        print(f'Couldn\'t load cog {cog}')

@bot.event
async def on_message(message):
    if message.author.id in load.__blacklist__ and load.__prefix__ in message.clean_content:
        embed=discord.Embed(color=load.__embedred__, description=f'You\'re blacklisted! So you aren\'t allowed to use commands. Contact the bot owner for more informations.')
        await message.channel.send(embed=embed)
        return
    if bot.user.mentioned_in(message) and message.mention_everyone is False:
        if load.__prefix__ not in message.content:
            embed=discord.Embed(color=load.__embedblue__, description=f'Use {load.__prefix__}help to see all commands.')
            await message.channel.send(embed=embed)
    await bot.process_commands(message)

@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.errors.DisabledCommand):
        embed=discord.Embed(color=load.__embedred__, title=f'{load.__warning__} Command Error', description=f'This command is temporarily disabled.')
        await ctx.send(embed=embed)
    elif isinstance(error, commands.BotMissingPermissions):
        if len(error.missing_perms) < 1:
            perms = ''.join(error.missing_perms)
        else:
            perms = ', '.join(error.missing_perms)
        embed=discord.Embed(color=load.__embedred__, title=f'{load.__warning__} Permission Error', description=f'I need the `{perms}` permission(s) to execute this command.')
        await ctx.send(embed=embed)
    elif isinstance(error, commands.errors.MissingPermissions):
        if len(error.missing_perms) < 1:
            perms = ''.join(error.missing_perms)
        else:
            perms = ', '.join(error.missing_perms)
        embed=discord.Embed(color=load.__embedred__, title=f'{load.__warning__} Permission Error', description=f'You need the `{perms}` permission(s) to execute this command.')
        await ctx.send(embed=embed)
    elif isinstance(error, commands.errors.MissingRequiredArgument):
        args = f'{error.param}'.replace(':str', '')
        embed=discord.Embed(color=load.__embedred__, title=f'{load.__warning__} Argument Error', description=f'You\'ve missed the required `{args}` argument.'.capitalize())
        await ctx.send(embed=embed)
    elif isinstance(error, commands.errors.NotOwner):
        embed=discord.Embed(color=load.__embedred__, title=f'{load.__warning__} Permission Error', description=f'You\'re not my owner.'.capitalize())
        await ctx.send(embed=embed)
    else:
        embed=discord.Embed(color=load.__embedred__,title=f'{load.__warning__} Error', description=f'{error}.'.capitalize())
        await ctx.send(embed=embed)

@bot.event
async def on_member_join(member):
    try:
        await member.add_roles(discord.utils.get(member.guild.roles, id=455436549140512769))
    except Exception as e:
        raise e
    else:
        embed=discord.Embed(color=load.__embedgreen__, description=f'Welcome to the {member.guild} guild. You got automatically assigned to the autorole, have fun!')
        member.send(embed=embed)

bot.run(load.__token__)