# Custom Server Emotes

__poll_emotes__ = [
    '1️⃣',
    '2️⃣',
    '3️⃣',
    '4️⃣',
    '5️⃣',
    '6️⃣',
    '7️⃣',
    '8️⃣',
    '9️⃣',
    '🔟'
]

__success__ = ':check:452826301078700045'
__error__ = ':xmark:452826303574441985'
__online__ = ':online:452826302546968579'
__away__ = ':away:452826300814589993'
__dnd__ = ':dnd:452826301393534976'
__offline__ = ':offline:452826302886576168'
__streaming__ = ':streaming:452826303905660928'
__invisible__ = ':invisible:452826302857347082'
__youtube__ = ':youtube:452826304195330069'
__discord__ = ':discord:452826301485809674'
__staff__ = ':staff:452826303066800131'
__warning__ = '⚠'