import datetime
import psutil
import random
import asyncio
import aiohttp
import discord
from discord.ext import commands
import load
import os

class utility:

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['git', 'github', 'bit'])
    async def bitbucket(self, ctx):
        embed=discord.Embed(color=load.__embedblue__, description=f'Here\'s the [Bitbucket repository](https://bitbucket.org/nfox1337/byte/src/) of the bot. It could be private at the time of seing')
        await ctx.send(embed=embed)
        
    @commands.command()
    async def info(self, ctx):
        '''Info about the bot'''
        ram=psutil.virtual_memory()
        embed=discord.Embed(color=load.__embedblue__)
        embed.add_field(name='Guilds', value=len(self.bot.guilds), inline=True)
        embed.add_field(name='RAM', value=f'{ram.percent}%', inline=True)
        embed.add_field(name='Commands', value=len(self.bot.commands), inline=True)
        embed.add_field(name='CPU', value=f'{psutil.cpu_percent()}%', inline=True)
        embed.add_field(name='Channels', value=str(len([c for c in self.bot.get_all_channels()])), inline=True)
        embed.add_field(name='Cores', value=f'{psutil.cpu_count()}', inline=True)
        embed.add_field(name='Users', value=str(len([c for c in self.bot.get_all_members()])), inline=True)
        embed.add_field(name='Library', value='[discord.py](https://github.com/Rapptz/discord.py)', inline=True)
        await ctx.send(embed=embed)

    @commands.command()
    async def avatar(self, ctx, member : discord.Member):
        try:
            embed=discord.Embed(color=load.__embedblue__)
            embed.set_author(name=f'Avatar from {member.display_name}')
            embed.set_image(url=member.avatar_url)
            await ctx.send(embed=embed)
        except Exception as e:
            raise e

    @commands.command()
    async def py(self, ctx, *, code):
        '''Quick command to edit into a codeblock.'''
        try:
            await ctx.message.delete()
            await ctx.send(f'```python\n{code}\n```')
        except Exception as e:
            raise e
    
    @commands.command()
    async def js(self, ctx, *, code):
        '''Quick command to edit into a codeblock.'''
        try:
            await ctx.message.delete()
            await ctx.send(f'```js\n{code}\n```')
        except Exception as e:
            raise e

    @commands.command()
    async def quote(self, ctx, id : int):
        '''Show the content of message by the id, not working with embeds atm'''
        try:
            for channel in self.bot.get_all_channels():
                try:
                    end = await channel.get_message(id)
                except Exception:
                    continue
            embed=discord.Embed(color=load.__embedblue__)
            embed.add_field(name='Guild', value=f'{end.guild}', inline=False)
            embed.add_field(name='Channel', value=f'{end.channel}', inline=False)
            embed.add_field(name='Author', value=f'{end.author}', inline=False)
            embed.add_field(name='Content', value=f'```{end.content}```', inline=False)
            await ctx.send(embed=embed)
        except Exception as e:
            raise e

def setup(bot):
    bot.add_cog(utility(bot))