import datetime
import psutil
import random
import asyncio
import aiohttp
import discord
from discord.ext import commands
import load
import os


class fun:

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def qr(self, ctx, *, search : str):
        '''Generates a QR code'''
        url = f'https://api.qrserver.com/v1/create-qr-code/?size=300x300x&outputformat=json&data={search}'
        if ' ' in search:
            raise Exception('API doen\'t support spaces')
        else:
            try:
                embed=discord.Embed(color=load.__embedgray__)
                embed.set_image(url=url)
                embed.set_footer(text=f'QR code generated with the goqr api')
                await ctx.send(embed=embed)
            except Exception as e:
                raise e

def setup(bot):
    bot.add_cog(fun(bot))
